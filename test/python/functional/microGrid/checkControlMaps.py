# Copyright (C) 2018 The Regents of the University of California
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
# Authors - Aditya Maheshwari and Mike Ludkovski
#           Department of Statistics and Applied Probability,
#           University of California at Santa Barbara

from __future__ import division
import numpy as np
import math
import matplotlib
# matplotlib.use('Agg')
import matplotlib.pyplot as plt
import time

import parameters as bv
import simulateState_new as simX
import valueNext_new as fv


import StOptGrids
import StOptReg
import StOptGeners



if __name__ == "__main__":

    outSampleSimul = 10000

    param = bv.basicVariables()

    condExpFilePath = 'condExp_rmcType_gd_regType_piecewiseLinear'
    
    I0 = np.random.uniform(param.I_minMax[0],param.I_minMax[1],(outSampleSimul,1))
    Xt = np.random.uniform(-8,8,(outSampleSimul,1))
    
    r,c = Xt.shape

    It = np.zeros((r,2)) # inventory evolution
    It[:,0] = I0[:,0]

    Dt = np.zeros((r,2), dtype=np.int) # clock 
    Dt[:,0] = 1  #initial regime

    St = np.zeros((r,c)) # stores Xt - dt - Bt
    Bt = np.zeros((r,c)) # battery output
    dt = np.zeros((r,c)) # diesel output

    archiveToRead =  StOptGeners.BinaryFileArchive(condExpFilePath,"r")


    contValues = archiveToRead.readGridAndRegressedValue(20,"toStore")

    for sim in range(outSampleSimul):
        _, dt[sim, 0], St[sim, 0], It[sim, 1] ,Bt[sim, 0] = fv.findOptimalControl(Xt[sim,0], It[sim,0], Dt[sim,0], contValues[0], param)


    plt.scatter(Xt[:,0],It[:,0],c=dt[:,0])
    plt.show()
        
