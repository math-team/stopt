// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef  MEANREVERTINGWRAP_H
#define  MEANREVERTINGWRAP_H
#include "StOpt/core/grids/OneDimRegularSpaceGrid.h"
#include "StOpt/core/grids/OneDimData.h"
#include "StOpt/python/Pybind11VectorAndList.h"
#include "test/c++/python/FutureCurveWrap.h"
#include "test/c++/tools/simulators/MeanRevertingSimulator.h"
#include <Python.h>

/** \file MeanRevertingWrap.h
 *  Defines the wrapping for the  Mean Reverting Simulator
 * \author Xavier Warin
 */

/// \class MeanRevertingWrap MeanReverting Wrap.h
/// Simulator wrap
class MeanRevertingWrap: public MeanRevertingSimulator< StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> >
{
public :
    /// \brief  Constructor
    /// \param curve    future curve given
    /// \param p_sigma  Volatility vector
    /// \param p_mr     Mean reverting coefficient
    /// \param p_r      Interest rate
    /// \param p_T      Maturity
    /// \param p_nbStep     Number of time step to simulate
    /// \param p_nbSimul    Number of Monte Carlo simulations
    /// \param p_bForward   is it a forward simulator (or a backward one)?
    MeanRevertingWrap(const FutureCurve &curve, const Eigen::VectorXd    &p_sigma,   const Eigen::VectorXd    &p_mr,  const double &p_r,
                      const double &p_T,   const size_t &p_nbStep,  const size_t &p_nbSimul, const bool &p_bForward) :
        MeanRevertingSimulator< StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> >(std::make_shared< StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> >(static_cast<StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> >(curve)), p_sigma,   p_mr, p_r,
                p_T,   p_nbStep,  p_nbSimul, p_bForward) {}
};

#endif /*MeanRevertingWrap.h */
