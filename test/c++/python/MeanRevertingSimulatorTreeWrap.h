// Copyright (C) 2019 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef  MEANREVERTINGSIMULATORTREEWRAP_H
#define  MEANREVERTINGSIMULATORTREEWRAP_H
#include "StOpt/core/grids/OneDimRegularSpaceGrid.h"
#include "StOpt/core/grids/OneDimData.h"
#include "StOpt/python/Pybind11VectorAndList.h"
#include "StOpt/python/BinaryFileArchiveStOpt.h"
#include "test/c++/python/FutureCurveWrap.h"
#include "test/c++/tools/simulators/MeanRevertingSimulatorTree.h"
#include <Python.h>

/** \file MeanRevertingSimulatorTreeWrap.h
 *  Defines the wrapping for the  Mean Reverting Simulator with tree
 * \author Xavier Warin
 */

/// \class MeanRevertingSimulatorTreeWrap MeanRevertingSimulatorTreeWrap.h
/// Simulator wrap
class MeanRevertingSimulatorTreeWrap: public MeanRevertingSimulatorTree< StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> >
{
public :
    /// \brief  Constructor backward
    /// \param p_binForTree archive describing tree
    /// \param curve    future curve given
    /// \param p_sigma  Volatility vector
    /// \param p_mr     Mean reverting coefficient
    MeanRevertingSimulatorTreeWrap(const std::shared_ptr<BinaryFileArchiveStOpt>   &p_binForTree, const FutureCurve &p_curve, const double   &p_sigma,   const double   &p_mr) :
        MeanRevertingSimulatorTree< StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> >(std::dynamic_pointer_cast<gs::BinaryFileArchive>(p_binForTree), std::make_shared< StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> >(static_cast<StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> >(p_curve)), p_sigma,   p_mr) {}

    /// \brief  Constructor forward
    /// \param p_binForTree archive describing tree
    /// \param p_curve    future curve given
    /// \param p_sigma  Volatility vector
    /// \param p_mr     Mean reverting coefficient
    /// \param p_nbSimul number of simulations in forward
    MeanRevertingSimulatorTreeWrap(const std::shared_ptr<BinaryFileArchiveStOpt>   &p_binForTree, const FutureCurve &p_curve, const double   &p_sigma,   const double   &p_mr, const int &p_nbSimul) :
        MeanRevertingSimulatorTree< StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> >(std::dynamic_pointer_cast<gs::BinaryFileArchive>(p_binForTree), std::make_shared< StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> >(static_cast<StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> >(p_curve)), p_sigma,   p_mr, p_nbSimul) {}
};

#endif /*MeanRevertingSimulatorTreeWrap.h */
