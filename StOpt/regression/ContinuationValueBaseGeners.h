// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef CONTINUATIONVALUEBASEGENERS_H
#define CONTINUATIONVALUEBASEGENERS_H
#include "StOpt/regression/ContinuationValueBase.h"
#include <geners/AbsReaderWriter.hh>
#include <geners/associate_serialization_factory.hh>

/** \file ContinuationValueBaseGeners.h
 * \brief Base class mapping with geners to archive ContinuationValueBase pointer type
 * \author  Xavier Warin
 */

/// \¢lass
///  I/O factory for classes derived from .
// Note publication of the base class and absence of public constructors.
class SerializationFactoryForContinuationValueBase : public gs::DefaultReaderWriter<StOpt::ContinuationValueBase>
{
    typedef DefaultReaderWriter<StOpt::ContinuationValueBase> Base;
    friend class gs::StaticReaderWriter<SerializationFactoryForContinuationValueBase>;
    SerializationFactoryForContinuationValueBase();
};

// SerializationFactoryForContinuationValueBase wrapped into a singleton
typedef gs::StaticReaderWriter<SerializationFactoryForContinuationValueBase> StaticSerializationFactoryForContinuationValueBase;

gs_specialize_class_id(StOpt::ContinuationValueBase, 1)
gs_declare_type_external(StOpt::ContinuationValueBase)
gs_associate_serialization_factory(StOpt::ContinuationValueBase, StaticSerializationFactoryForContinuationValueBase)

#endif
