// Copyright (C) 2023 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef  CONTINUATIONVALUEDETGENERS_H
#define  CONTINUATIONVALUEDETGENERS_H
#include <Eigen/Dense>
#include "geners/GenericIO.hh"
#include "StOpt/regression/ContinuationValueDet.h"
#include "StOpt/regression/ContinuationValueBaseGeners.h"
#include "StOpt/core/grids/SpaceGridGeners.h"
#include "StOpt/core/grids/RegularSpaceGridGeners.h"
#include "StOpt/core/grids/GeneralSpaceGridGeners.h"
#include "StOpt/core/grids/SparseSpaceGridNoBoundGeners.h"
#include "StOpt/core/grids/SparseSpaceGridBoundGeners.h"

/** \file ContinuationValueDetGeners.h
 * \brief Define non intrusive serialization with random access
*  \author Xavier Warin
 */

// Concrete reader/writer for class ContinuationValueDet
// Note publication of ContinuationValueDet as "wrapped_type".
struct ContinuationValueDetGeners: public gs::AbsReaderWriter<StOpt::ContinuationValueBase>
{
    typedef StOpt::ContinuationValueBase wrapped_base;
    typedef StOpt::ContinuationValueDet wrapped_type;

    // Methods that have to be overridden from the base
    bool write(std::ostream &, const wrapped_base &, bool p_dumpId) const override;
    wrapped_type *read(const gs::ClassId &p_id, std::istream &p_in) const override;

    // The class id for ContinuationValueDet  will be needed both in the "read" and "write"
    // methods. Because of this, we will just return it from one static
    // function.
    static const gs::ClassId &wrappedClassId();
};

gs_specialize_class_id(StOpt::ContinuationValueDet, 1)
gs_declare_type_external(StOpt::ContinuationValueDet)
gs_associate_serialization_factory(StOpt::ContinuationValueDet, StaticSerializationFactoryForContinuationValueBase)


#endif/*  CONTINUATIONVALUEDETGENERS_H */
