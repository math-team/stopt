Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: StOpt library
Source: https://gitlab.com/stochastic-control/StOpt
Files-Excluded: geners-*
                utils/bzip2-master.zip
                utils/zlib-master.zip
                .gitignore
Files-Excluded-texdoc: *.aux
                       *.dvi
                       *.log
                       *.out
                       *.pdf
                       *.toc
Comment: removing embedded zip sources, geners which has its own package,
 and latex outputs.

Files: *
Copyright: 2014-2021 EDF
License: LGPL-3+

Files: StOpt/regression/LaplacianGridKernelRegression.cpp
       StOpt/regression/LaplacianGridKernelRegression.h
       StOpt/regression/fastLaplacianKDE.cpp
       StOpt/regression/fastLaplacianKDE.h
       test/python/unit/regression/testLaplacianGridKernelRegression.py
Copyright: 2020 EDF
           2020 CSIRO Data61
License: LGPL-3+

Files: StOpt/regression/MultiVariateBasis.cpp
       StOpt/regression/MultiVariateBasis.h
Copyright: 2016 Jerome Lelong <jerome.lelong@imag.fr>
License: LGPL-3+

Files: test/c++/functional/testAmericanOptionForSparse.cpp
       test/c++/functional/testAmericanOptionForSparseProf.cpp
       test/c++/functional/testAmericanOptionProf.cpp
       test/c++/functional/testDemandSDDP.cpp
       test/c++/functional/testGasStorageGlobal.cpp
       test/c++/functional/testGasStorageGlobalMpi.cpp
       test/c++/functional/testGasStorageMpi.cpp
       test/c++/functional/testGasStorageSDDP.cpp
       test/c++/functional/testGasStorageSDDPTree.cpp
       test/c++/functional/testGasStorageSwitchCostMpi.cpp
       test/c++/functional/testGasStorageTreeMpi.cpp
       test/c++/functional/testGasStorageVaryingCavity.cpp
       test/c++/functional/testGasStorageVaryingCavityMpi.cpp
       test/c++/functional/testLake.cpp
       test/c++/functional/testLakeIid.cpp
       test/c++/functional/testLakeMpi.cpp
       test/c++/functional/testReservoirWithInflowsSDDP.cpp
       test/c++/functional/testSemiLagragian1.cpp
       test/c++/functional/testSemiLagragian1Mpi.cpp
       test/c++/functional/testSemiLagragian2.cpp
       test/c++/functional/testSemiLagragian2Mpi.cpp
       test/c++/functional/testSemiLagragian3.cpp
       test/c++/functional/testSemiLagragian3Mpi.cpp
       test/c++/functional/testStorageWithInflowsSDDP.cpp
Copyright: 2016-2019 Fime
License: LGPL-3+

Files: test/c++/unit/regression/testMultiVariateBasis.cpp
Copyright: 2016 Jerome Lelong <jerome.lelong@imag.fr>
License: LGPL-3+

Files: test/python/functional/microGrid/*
       test/python/functional/microgridDEA/*
       test/python/functional/testMicrogrid.py
       test/python/functional/testMicrogridDEA.py
Copyright: 2018 The Regents of the University of California, Michael Ludkovski and Aditya Maheshwari
License: LGPL-3+

Files: test/python/functional/microGrid/checkControlMaps.py
Copyright: 2018 The Regents of the University of California
License: LGPL-3+

Files: test/python/functional/microgridDEA/sobol_lib.py
Copyright: 2011 John Burkardt
           2011 Corrado Chisari
           2011 Bennett Fox
License: Expat

Files: debian/*
Copyright: 2021 Pierre Gruet <pgt@debian.org>
License: LGPL-3+

License: LGPL-3+
 The StOpt library is a free software: you can redistribute it and/or modify
 it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU LESSER GENERAL PUBLIC LICENSE.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems the full text of the LGPL-3 can be found in
 /usr/share/common-licenses/LGPL-3

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject
 to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name(s) of the above copyright
 holders shall not be used in advertising or otherwise to promote the
 sale, use or other dealings in this Software without prior written
 authorization.
