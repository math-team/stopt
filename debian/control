Source: stopt
Maintainer: Debian Math Team <team+math@tracker.debian.org>
Uploaders: Pierre Gruet <pgt@debian.org>,
           Xavier Warin <xavier.warin@gmail.com>
Section: math
Priority: optional
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 13),
               dh-python,
Build-Depends-Arch: cmake,
                    coinor-libcoinutils-dev,
                    coinor-libosi-dev,
                    coinor-libclp-dev,
                    libboost-chrono-dev,
                    libboost-log-dev,
                    libboost-mpi-dev,
                    libboost-random-dev,
                    libboost-serialization-dev,
                    libboost-system-dev,
                    libboost-test-dev,
                    libboost-thread-dev,
                    libboost-timer-dev,
                    libeigen3-dev,
                    libgeners-dev,
                    mpi-default-dev,
                    pybind11-dev,
                    python3-dev,
                    python3-docutils,
                    python3-numpy
Build-Depends-Indep: texlive-fonts-extra <!nodoc>,
                     texlive-latex-recommended <!nodoc>,
                     texlive-plain-generic <!nodoc>,
                     texlive-pstricks <!nodoc>,
                     texlive-science <!nodoc>
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/math-team/stopt
Vcs-Git: https://salsa.debian.org/math-team/stopt.git
Homepage: https://gitlab.com/stochastic-control/StOpt/
Rules-Requires-Root: no

Package: libstopt5t64
Provides: ${t64:Provides}
Replaces: libstopt5
Breaks: libstopt5 (<< ${source:Version})
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: stopt-doc (= ${binary:Version})
Description: library for stochastic optimization problems (shared library)
 The STochastic OPTimization library (StOpt) aims at providing tools in C++ for
 solving some stochastic optimization problems encountered in finance or in the
 industry. Different methods are available:
  - dynamic programming methods based on Monte Carlo with regressions (global,
  local, kernel and sparse regressors), for underlying states following some
  uncontrolled Stochastic Differential Equations;
  - dynamic programming with a representation of uncertainties with a tree:
  transition problems are here solved by some discretizations of the commands,
  resolution of LP with cut representation of the Bellman values;
  - Semi-Lagrangian methods for Hamilton Jacobi Bellman general equations for
  underlying states following some controlled Stochastic Differential
  Equations;
  - Stochastic Dual Dynamic Programming methods to deal with stochastic stock
  management problems in high dimension. Uncertainties can be given by Monte
  Carlo and can be represented by a state with a finite number of values
  (tree);
  - Some branching nesting methods to solve very high dimensional non linear
  PDEs and some appearing in HJB problems. Besides some methods are provided
  to solve by Monte Carlo some problems where the underlying stochastic state
  is controlled.
  For each method, a framework is provided to optimize the problem and then
  simulate it out of the sample using the optimal commands previously computed.
  Parallelization methods based on OpenMP and MPI are provided in this
  framework permitting to solve high dimensional problems on clusters.
 The library should be flexible enough to be used at different levels depending
 on the user's willingness.
 .
 This package contains the shared libraries: one which allows for
 multithreading (libstopt-mpi) and one which does not (libstopt).

Package: libstopt-dev
Architecture: any
Section: libdevel
Depends: libstopt5t64 (= ${binary:Version}),
         coinor-libcoinutils-dev,
         coinor-libosi-dev,
         coinor-libclp-dev,
         libboost-chrono-dev,
         libboost-log-dev,
         libboost-mpi-dev,
         libboost-random-dev,
         libboost-serialization-dev,
         libboost-system-dev,
         libboost-test-dev,
         libboost-thread-dev,
         libboost-timer-dev,
         libeigen3-dev,
         libgeners-dev,
         mpi-default-dev,
         ${misc:Depends}
Suggests: stopt-doc (= ${binary:Version})
Description: library for stochastic optimization problems (development package)
 The STochastic OPTimization library (StOpt) aims at providing tools in C++ for
 solving some stochastic optimization problems encountered in finance or in the
 industry. Different methods are available:
  - dynamic programming methods based on Monte Carlo with regressions (global,
  local, kernel and sparse regressors), for underlying states following some
  uncontrolled Stochastic Differential Equations;
  - dynamic programming with a representation of uncertainties with a tree:
  transition problems are here solved by some discretizations of the commands,
  resolution of LP with cut representation of the Bellman values;
  - Semi-Lagrangian methods for Hamilton Jacobi Bellman general equations for
  underlying states following some controlled Stochastic Differential
  Equations;
  - Stochastic Dual Dynamic Programming methods to deal with stochastic stock
  management problems in high dimension. Uncertainties can be given by Monte
  Carlo and can be represented by a state with a finite number of values
  (tree);
  - Some branching nesting methods to solve very high dimensional non linear
  PDEs and some appearing in HJB problems. Besides some methods are provided
  to solve by Monte Carlo some problems where the underlying stochastic state
  is controlled.
  For each method, a framework is provided to optimize the problem and then
  simulate it out of the sample using the optimal commands previously computed.
  Parallelization methods based on OpenMP and MPI are provided in this
  framework permitting to solve high dimensional problems on clusters.
 The library should be flexible enough to be used at different levels depending
 on the user's willingness.
 .
 This package contains the headers and the static libraries (libstopt-mpi
 which allows for multithreading, and libstopt which does not).

Package: stopt-examples
Architecture: all
Depends: ${misc:Depends}
Suggests: g++,
          libstopt-dev,
          python3-numpy,
          python3-stopt
Enhances: libstopt-dev,
          python3-stopt
Multi-Arch: foreign
Description: library for stochastic optimization problems (programs examples)
 This package provides some programs written to solve mathematical problems
 using the StOpt library. The source code is provided, examples are available
 in C++ and in Python. C++ source code has to be built against the libstopt-dev
 package if one wants to run it.

Package: python3-stopt
Architecture: any
Section: python
Depends: ${python3:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Provides: ${python3:Provides}
Description: library for stochastic optimization problems (Python 3 bindings)
 The STochastic OPTimization library (StOpt) aims at providing tools in C++ for
 solving some stochastic optimization problems encountered in finance or in the
 industry. Python 3 bindings are provided by this package in order to allow one
 to use the C++ library in a Python code.

Package: stopt-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Suggests: libstopt5t64 (= ${binary:Version}),
          libstopt-dev (= ${binary:Version}),
          stopt-examples (= ${binary:Version}),
          python3-stopt (= ${binary:Version})
Multi-Arch: foreign
Description: library for stochastic optimization problems (documentation)
 The STochastic OPTimization library (StOpt) aims at providing tools in C++ for
 solving some stochastic optimization problems encountered in finance or in the
 industry. Python 3 bindings are also provided in order to allow one to use the
 C++ library in a Python code.
 .
 This package contains the documentation about the type of problems that can be
 solved, the mathematical framework, its implementation, and the examples.
